// js code for loader 

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 2000);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}

// data url

let dataUrl = "https://restcountries.com/v3.1/all";
fetchdata()

//fetching relevant data from url

function fetchdata() {
    fetch(dataUrl)
        .then((data) => data.json())
        .then((data) => {
            let count = 0;
            return data.reduce((accumulator, currentvalue) => {
                let flag = currentvalue.flags.png;
                let country = currentvalue.name.official;
                let population = currentvalue.population;
                let region = currentvalue.region;
                let capital = currentvalue.capital;
                let subregion = currentvalue.subregion;
                let languages = currentvalue.languages;
                let currencies = currentvalue.currencies;
                let borders = currentvalue.borders

                accumulator[count] = {};
                accumulator[count]["flag"] = flag;
                accumulator[count]["country"] = country;
                accumulator[count]["population"] = population;
                accumulator[count]["region"] = region;
                accumulator[count]["capital"] = capital;
                accumulator[count]["region"] = region;
                accumulator[count]["subregion"] = subregion;
                accumulator[count]["languages"] = languages;
                accumulator[count]["currencies"] = currencies;
                accumulator[count]["borders"] = borders;

                count++;
                return accumulator;
            }, []);
        })
        .then((data) => {
            setTimeout(() => {
                countryDisplay(data);
                return data;
            }, 2000)

        })
        .catch((err) => {
            console.log(err);
            alert("some error in data file");
        })
}

function countryDisplay(data) {

    // creating heading
    const body = document.body;
    const headingDiv = document.createElement("div");
    headingDiv.style.backgroundColor="white";
    const heading = document.createElement("h1");
    heading.innerText = "where in the world ";
    headingDiv.appendChild(heading);
    body.insertBefore(headingDiv, document.getElementById("flex-container"));
    body.style.backgroundColor = "#F5F5DC";
    headingDiv.style.display = "flex";
    headingDiv.style.marginBottom = "4vh";

    //div for creating region filter

    let searchAndFilterDiv = document.createElement("div");
    let searchCountryBar = document.createElement("label");
    let selecter = document.createElement("select");
    searchCountryBar.innerText = "filter by region : ";
    let option0 = document.createElement("option");
    option0.innerText = "All regions";
    selecter.appendChild(option0);
    let option1 = document.createElement("option");
    option1.innerText = "Africa";
    selecter.appendChild(option1);
    let option2 = document.createElement("option");
    option2.textContent = "Asia";
    selecter.appendChild(option2);
    let option3 = document.createElement("option");
    option3.textContent = "Americas";
    selecter.appendChild(option3);
    let option4 = document.createElement("option");
    option4.textContent = "Europe";
    selecter.appendChild(option4);
    let option5 = document.createElement("option");
    option5.textContent = "Oceania";
    selecter.appendChild(option5);


    searchCountryBar.appendChild(selecter);
    searchAndFilterDiv.appendChild(searchCountryBar);
    body.insertBefore(searchAndFilterDiv, document.getElementById("flex-container"));


    // search and filter div styling

    searchAndFilterDiv.style.display = "flex";
    searchAndFilterDiv.style.margin = "10vh";
    searchCountryBar.style.marginLeft = "auto";

    let section = document.getElementById("flex-container");

    //styling the container
    section.style.display = "flex";
    section.style.flexWrap = "wrap";
    section.style.rowGap = "10vh";
    section.style.columnGap = "4.5vw";
    section.style.padding = "0vh 7vw ovh 7vw";
    section.style.marginLeft = "5vw";
    section.style.marginRight = "5vw";





    for (let country of data) {

        let newdiv = document.createElement("div");  //creating div for each individual country
        newdiv.className = "country-div";
        newdiv.style.width = "21%";
        newdiv.style.backgroundColor = "white";            // styling individual country
        section.appendChild(newdiv);


        let countryImage = document.createElement("img");  // creating image of flag
        countryImage.style.height = "12rem";
        countryImage.style.width = "100%";
        countryImage.className = "flag";
        countryImage.style.marginBottom = "0.7rem";
        countryImage.setAttribute("src", country.flag);
        newdiv.appendChild(countryImage);

        let countryNameDiv = document.createElement("div");   //creating div  for country
        countryNameDiv.className = "country-name";
        countryNameDiv.innerText = country.country;
        countryNameDiv.style.fontSize = "1.5rem";
        countryNameDiv.style.fontWeight = "600";
        countryNameDiv.style.marginBottom = "1.5rem";
        countryNameDiv.style.marginLeft = "0.8rem";

        newdiv.appendChild(countryNameDiv);

        let countryCapitalDiv = document.createElement("div");    //creating div for country-capital
        countryCapitalDiv.className = "country-capital";
        countryCapitalDiv.innerText = `capital : ${country.capital}`;
        countryCapitalDiv.style.marginLeft = "0.8rem";
        newdiv.appendChild(countryCapitalDiv);


        let countryRegionDiv = document.createElement("div");    //creating div for country-region
        countryRegionDiv.className = "country-Region";
        countryRegionDiv.innerText = `Region : ${country.region}`;
        countryRegionDiv.style.marginLeft = "0.8rem";
        newdiv.appendChild(countryRegionDiv);

        let countryPopulationDiv = document.createElement("div");     //creating div for country-population
        countryPopulationDiv.className = "country-Population";
        countryPopulationDiv.innerText = `population : ${country.population}`;
        countryPopulationDiv.style.marginLeft = "0.8rem";
        newdiv.appendChild(countryPopulationDiv);



    }



    //filtering div 

    const h1 = document.querySelector("select");

    h1.addEventListener("click", function (e) {
        let clickedRegion = e.target.value;
        console.log(clickedRegion);
        let filteredArr = data.filter((country) => {
            if (country.region == clickedRegion) {
                return country;
            }
        })
        console.log(filteredArr)
        let a1 = document.querySelectorAll(".country-div");
        a1.forEach((ele) => {
            ele.remove();
        })
        if (clickedRegion == "All regions") {
            filteredArr = data;
        }


        filteredArr.forEach(country => {
            let newdiv = document.createElement("div");  //creating div for each individual country
            newdiv.className = "country-div";
            //newdiv.data.modal.target="#modal"
            newdiv.style.width = "21%";
            newdiv.style.backgroundColor = "white";              // styling individual country
            section.appendChild(newdiv);

            let countryImage = document.createElement("img");  // creating image of flag
            countryImage.style.height = "12rem";
            countryImage.style.width = "100%";
            countryImage.className = "flag";
            countryImage.style.marginBottom = "0.7rem";
            countryImage.setAttribute("src", country.flag);
            newdiv.appendChild(countryImage);

            let countryNameDiv = document.createElement("div");   //creating div  for country
            countryNameDiv.className = "country-name";
            countryNameDiv.innerText = country.country;
            countryNameDiv.style.fontSize = "1.5rem";
            countryNameDiv.style.fontWeight = "600";
            countryNameDiv.style.marginBottom = "1.5rem";
            countryNameDiv.style.marginLeft = "0.8rem";

            newdiv.appendChild(countryNameDiv);

            let countryCapitalDiv = document.createElement("div");    //creating div for country-capital
            countryCapitalDiv.className = "country-capital";
            countryCapitalDiv.innerText = `capital : ${country.capital}`;
            countryCapitalDiv.style.marginLeft = "0.8rem";
            newdiv.appendChild(countryCapitalDiv);


            let countryRegionDiv = document.createElement("div");    //creating div for country-region
            countryRegionDiv.className = "country-Region";
            countryRegionDiv.innerText = `Region : ${country.region}`;
            countryRegionDiv.style.marginLeft = "0.8rem";
            newdiv.appendChild(countryRegionDiv);

            let countryPopulationDiv = document.createElement("div");     //creating div for country-population
            countryPopulationDiv.className = "country-Population";
            countryPopulationDiv.innerText = `population : ${country.population}`;
            countryPopulationDiv.style.marginLeft = "0.8rem";
            newdiv.appendChild(countryPopulationDiv);


        });

        //for filtered regions single country display

        const countryFilter = document.querySelectorAll(".country-div");
        countryFilter.forEach((ele) => {
            ele.addEventListener("click", function (e) {
                console.log(e.target);
                console.log(e.target.src);
                let details = data.find((country) => {
                    if (country.flag == e.target.src) {
                        return country;
                    }
                })
                console.log(details)

                let lang = details.languages;
                let langArr = Object.values(lang);
                let currency = details.currencies;
                let currArr = Object.values(currency);
                let countryCurrency = Object.values(currArr[0]);
                console.log(countryCurrency[0]);


                let modalBodyContainer = document.getElementById("modal-body");



                let modaldiv = document.createElement("div");
                modaldiv.className = "modalmainclass";
                modaldiv.style.width = "100%";
                modaldiv.style.display = "flex";
                modaldiv.style.flexDirection="column";
                modaldiv.style.justifyContent = "space-between";



                let detailsdiv = document.createElement("div");

                let countname = document.createElement("div");
                let imgdiv = document.createElement("div");
                let populatdiv = document.createElement("div");
                let capdiv = document.createElement("div");
                let langdiv = document.createElement("div");
                let currencydiv = document.createElement("div");

                let bordersdiv = document.createElement("div");
                bordersdiv.innerText=`Borders : ${details.borders}`;
                if(details.borders==undefined){
                    bordersdiv.innerText="Borders: NIL"; 

                }

                imgdiv.style.width = "10%";
                imgdiv.style.margin = "10%";

            
                detailsdiv.style.marginLeft = "10%";



                let countImage = document.createElement("img");
                countImage.setAttribute("src", details.flag);
                imgdiv.appendChild(countImage)

                countname.innerText = details.country;
                countname.style.fontSize = "100%";

                populatdiv.innerText = `Country-population:${details.population}`;

                capdiv.innerText = `Capital:${details.capital}`;

                langdiv.innerText = `Lanuages: ${langArr}`;

                currencydiv.innerText = `Currency: ${countryCurrency[0]}`;

                capdiv.style.marginTop = "15px";
                populatdiv.style.marginTop = "15px";
                currencydiv.style.marginTop = "15px";
                langdiv.style.marginTop = "15px";
                bordersdiv.style.marginTop = "15px";




                modaldiv.appendChild(imgdiv);
                modaldiv.appendChild(detailsdiv);



                detailsdiv.appendChild(countname);
                detailsdiv.appendChild(populatdiv);
                detailsdiv.appendChild(capdiv);
                detailsdiv.appendChild(langdiv);
                detailsdiv.appendChild(currencydiv);
                detailsdiv.appendChild(bordersdiv);

                modalBodyContainer.appendChild(modaldiv);
                openModal(modal)
            })
        })
    })

    //for main page single country display


    const countryFilter = document.querySelectorAll(".country-div");
    countryFilter.forEach((ele) => {
        ele.addEventListener("click", function (e) {
            //window.open("./popup.html")
            console.log(e.target);
            console.log(e.target.src);
            let details = data.find((country) => {
                if (country.flag == e.target.src) {
                    return country;
                }
            })
            console.log(details);
            // popupdisplay();

            let lang = details.languages;
            let langArr = Object.values(lang);
            let currency = details.currencies;
            let currArr = Object.values(currency);
            let countryCurrenncy = Object.values(currArr[0]);
            console.log(countryCurrenncy[0]);

            let modalBodydiv = document.getElementById("modal-body");

            let modaldiv = document.createElement("div");
            modaldiv.className = "modalmainclass";
            modaldiv.style.width = "100%";
            modaldiv.style.display = "flex";
            modaldiv.style.flexDirection="column";
            modaldiv.style.justifyContent = "space-between";
            


            let detailsdiv = document.createElement("div");

            let countname = document.createElement("div");
            let imgdiv = document.createElement("div");
            let populatdiv = document.createElement("div");
            let capdiv = document.createElement("div");
            let langdiv = document.createElement("div");
            let currencydiv = document.createElement("div");

            let bordersdiv = document.createElement("div");
            bordersdiv.innerText=`Borders: ${details.borders}`;

            imgdiv.style.width = "10%";
            imgdiv.style.margin = "10%";

            detailsdiv.style.marginLeft = "10%";

            capdiv.style.marginTop = "15px";
            populatdiv.style.marginTop = "15px";
            currencydiv.style.marginTop = "15px";
            langdiv.style.marginTop = "15px";
            bordersdiv.style.marginTop = "15px";



            let countImage = document.createElement("img");
            countImage.setAttribute("src", details.flag);
            imgdiv.appendChild(countImage);

    

            countname.innerText = details.country;
            countname.style.fontSize = "bold";

            populatdiv.innerText = `Country-population:${details.population}`;

            capdiv.innerText = `Capital:${details.capital}`;

            langdiv.innerText = `Lanuages: ${langArr}`;

            currencydiv.innerText = `Currency: ${countryCurrenncy[0]}`;

            modaldiv.appendChild(imgdiv);
            modaldiv.appendChild(detailsdiv);

            detailsdiv.appendChild(countname);
            detailsdiv.appendChild(populatdiv);
            detailsdiv.appendChild(capdiv);
            detailsdiv.appendChild(langdiv);
            detailsdiv.appendChild(currencydiv);
            detailsdiv.appendChild(bordersdiv);

            modalBodydiv.appendChild(modaldiv);
            openModal(modal)
        })

    })
}

const openModalButtons = document.querySelectorAll('[data-modal-target]');
const closeModalButtons = document.querySelectorAll('[data-close-button]');
const overlay = document.getElementById('overlay');

overlay.addEventListener('click', () => {
    const modals = document.querySelectorAll('.modal.active')
    modals.forEach(modal => {
        closeModal(modal)
    })
})




function openModal(modal) {
    console.log("inside")
    if (modal == null) return
    modal.classList.add('active')
    overlay.classList.add('active')
    setTimeout(()=>{
        document.addEventListener('click',(e)=>{
            if(e.target!=modal){
                closeModal(modal)
                
            }
        },1000)

    })
  

}

function closeModal(modal) {
    if (modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')

    let deleterModalmainClass = document.getElementsByClassName("modalmainclass")
    console.log(deleterModalmainClass);
    console.log(deleterModalmainClass.length);
    console.log(deleterModalmainClass[0]);
    let out = deleterModalmainClass[0];
    if(out){
        out.remove()

    }
    
}

document.addEventListener('keydown', function (event) {
    const key = event.key; // const {key} = event; in ES6+
    if (key === "Escape") {
        closeModal(modal)
    }
});











